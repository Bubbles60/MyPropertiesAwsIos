//
//  UserDetailVC.swift
//  MyProperties
//
//  Created by Donncha Finlay on 30/09/16.
//  Copyright © 2016 Donncha Finlay. All rights reserved.
//

import UIKit

class UserDetailVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        getUserFromAWS()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func getUserFromAWS() {
        /* 2. Make the request */
        let methodParameters = [EconUserFileClient.Constants.TokenKey: EconUserFileClient.Constants.TokenValue]
        let euc = EconUserFileClient()
        euc.taskForGETMethod(EconUserFileClient.Methods.User, parameters: methodParameters) { (results, error) in
            // Note this will test for an error in parsed data or get required info
            /* 3. Send the desired value(s) to completion handler */
            if let error = error {
                print(error)
                
            } else
            {
                guard let user = results["mail"] as? String else {
                    euc.displayError("Cannot find key response in result")
                    return
                }
                print(user)
                }
                // Initial result is not an array
               /* guard let teams = results["teams"]! as? [AnyObject!] else {
                    fc.displayError("Can not find the key Teams in \(results)")
                    return
                }
                // Teams is an array
                var i = 0
                for team in teams as! [AnyObject!] {
                    guard let teama = team["name"]! as? String else {
                        fc.displayError("Can not find the key name in \(team)")
                        return
                    }
                    var teamImage = "None"
                    if let flag = team["crestUrl"]! as? String {
                        teamImage = flag
                    }
                    self.myTeams.append((teama,teamImage))
                }*/
                
               
            }
        }
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

