/*
 * Copyright 2010-2016 Amazon.com, Inc. or its affiliates. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License.
 * A copy of the License is located at
 *
 *  http://aws.amazon.com/apache2.0
 *
 * or in the "license" file accompanying this file. This file is distributed
 * on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */

import Foundation
import AWSDynamoDB

let AWSSampleDynamoDBTableName = "PropertiesDFB"


class DDBDynamoDBManger : NSObject {
    class func describeTable() -> AWSTask {
        let dynamoDB = AWSDynamoDB.defaultDynamoDB()

        // See if the test table exists.
        let describeTableInput = AWSDynamoDBDescribeTableInput()
        describeTableInput.tableName = AWSSampleDynamoDBTableName
        return dynamoDB.describeTable(describeTableInput)
    }
    class func createTable() -> AWSTask {
        let dynamoDB = AWSDynamoDB.defaultDynamoDB()
        
        //Create the test table
        let hashKeyAttributeDefinition = AWSDynamoDBAttributeDefinition()
        hashKeyAttributeDefinition.attributeName = "Mobile"
        hashKeyAttributeDefinition.attributeType = AWSDynamoDBScalarAttributeType.S
        
        let hashKeySchemaElement = AWSDynamoDBKeySchemaElement()
        hashKeySchemaElement.attributeName = "Mobile"
        hashKeySchemaElement.keyType = AWSDynamoDBKeyType.Hash
        
        let rangeKeyAttributeDefinition = AWSDynamoDBAttributeDefinition()
        rangeKeyAttributeDefinition.attributeName = "City"
        rangeKeyAttributeDefinition.attributeType = AWSDynamoDBScalarAttributeType.S
        
        let rangeKeySchemaElement = AWSDynamoDBKeySchemaElement()
        rangeKeySchemaElement.attributeName = "City"
        rangeKeySchemaElement.keyType = AWSDynamoDBKeyType.Range
        
        //Add non-key attributes
        let priceAttrDef = AWSDynamoDBAttributeDefinition()
        priceAttrDef.attributeName = "Price"
        priceAttrDef.attributeType = AWSDynamoDBScalarAttributeType.N
        //
        //Add non-key attributes
        let noOfRoomsAttrDef = AWSDynamoDBAttributeDefinition()
        noOfRoomsAttrDef.attributeName = "NumberOfRooms"
        noOfRoomsAttrDef.attributeType = AWSDynamoDBScalarAttributeType.N
        
        let provisionedThroughput = AWSDynamoDBProvisionedThroughput()
        provisionedThroughput.readCapacityUnits = 5
        provisionedThroughput.writeCapacityUnits = 5
        
        //Create Global Secondary Index
        let rangeKeyArray = ["Price","NumberOfRooms"]
        var gsiArray = [AWSDynamoDBGlobalSecondaryIndex]()
        
        for rangeKey in rangeKeyArray {
            let gsi = AWSDynamoDBGlobalSecondaryIndex()
            
            let gsiHashKeySchema = AWSDynamoDBKeySchemaElement()
            gsiHashKeySchema.attributeName = "City"
            gsiHashKeySchema.keyType = AWSDynamoDBKeyType.Hash
            
            let gsiRangeKeySchema = AWSDynamoDBKeySchemaElement()
            gsiRangeKeySchema.attributeName = rangeKey
            gsiRangeKeySchema.keyType = AWSDynamoDBKeyType.Range
            
            let gsiProjection = AWSDynamoDBProjection()
            gsiProjection.projectionType = AWSDynamoDBProjectionType.All;
            
            gsi.keySchema = [gsiHashKeySchema,gsiRangeKeySchema];
            gsi.indexName = rangeKey;
            gsi.projection = gsiProjection;
            gsi.provisionedThroughput = provisionedThroughput;
            
            gsiArray.append(gsi)
        }
        
        //Create TableInput
        let createTableInput = AWSDynamoDBCreateTableInput()
        createTableInput.tableName = AWSSampleDynamoDBTableName;
        createTableInput.attributeDefinitions = [hashKeyAttributeDefinition, rangeKeyAttributeDefinition, priceAttrDef,noOfRoomsAttrDef]
        createTableInput.keySchema = [hashKeySchemaElement, rangeKeySchemaElement]
        createTableInput.provisionedThroughput = provisionedThroughput
        createTableInput.globalSecondaryIndexes = gsiArray
        
        return dynamoDB.createTable(createTableInput).continueWithSuccessBlock({ task -> AnyObject? in
            var localTask = task
            
            if ((localTask.result) != nil) {
                // Wait for up to 4 minutes until the table becomes ACTIVE.
                
                let describeTableInput = AWSDynamoDBDescribeTableInput()
                describeTableInput.tableName = AWSSampleDynamoDBTableName;
                localTask = dynamoDB.describeTable(describeTableInput)
                
                for _ in 0...15 {
                    localTask = localTask.continueWithSuccessBlock({ task -> AnyObject? in
                        let describeTableOutput:AWSDynamoDBDescribeTableOutput = task.result as! AWSDynamoDBDescribeTableOutput
                        let tableStatus = describeTableOutput.table!.tableStatus
                        if tableStatus == AWSDynamoDBTableStatus.Active {
                            return task
                        }
                        
                        sleep(15)
                        return dynamoDB .describeTable(describeTableInput)
                    })
                }
            }
            
            return localTask
        })
        
    }
}

  /*  class func createTable() -> AWSTask {
        let dynamoDB = AWSDynamoDB.defaultDynamoDB()

        //Create the test table
        
        let hashKeyAttributeDefinition = AWSDynamoDBAttributeDefinition()
        hashKeyAttributeDefinition.attributeName = "Address"
        hashKeyAttributeDefinition.attributeType = AWSDynamoDBScalarAttributeType.S
        
        let hashKeySchemaElement = AWSDynamoDBKeySchemaElement()
        hashKeySchemaElement.attributeName = "Address"
        hashKeySchemaElement.keyType = AWSDynamoDBKeyType.Hash
        
        let rangeKeyAttributeDefinition = AWSDynamoDBAttributeDefinition()

        

        //Add non-key attributes
        let numberOfRoomsAttrDef = AWSDynamoDBAttributeDefinition()
        numberOfRoomsAttrDef.attributeName = "NumberOfRooms"
        numberOfRoomsAttrDef.attributeType = AWSDynamoDBScalarAttributeType.N
        
        
        let ownerAttrDef = AWSDynamoDBAttributeDefinition()
        ownerAttrDef.attributeName = "Owner"
        ownerAttrDef.attributeType = AWSDynamoDBScalarAttributeType.S
        
        let photoURLAttrDef = AWSDynamoDBAttributeDefinition()
        photoURLAttrDef.attributeName = "PhotoURL"
        photoURLAttrDef.attributeType = AWSDynamoDBScalarAttributeType.S
        
        let priceAttrDef = AWSDynamoDBAttributeDefinition()
        priceAttrDef.attributeName = "Price"
        priceAttrDef.attributeType = AWSDynamoDBScalarAttributeType.N
        
        let selleAttrDef = AWSDynamoDBAttributeDefinition()
        selleAttrDef.attributeName = "Seller"
        selleAttrDef.attributeType = AWSDynamoDBScalarAttributeType.S
        
        
        
        

               let provisionedThroughput = AWSDynamoDBProvisionedThroughput()
        provisionedThroughput.readCapacityUnits = 5
        provisionedThroughput.writeCapacityUnits = 5

       

        //Create TableInput
        let createTableInput = AWSDynamoDBCreateTableInput()
        createTableInput.tableName = AWSSampleDynamoDBTableName;
        createTableInput.attributeDefinitions = [hashKeyAttributeDefinition,rangeKeyAttributeDefinition,numberOfRoomsAttrDef]
         createTableInput.keySchema = [hashKeySchemaElement]
         createTableInput.provisionedThroughput = provisionedThroughput
      
        return dynamoDB.createTable(createTableInput).continueWithSuccessBlock({ task -> AnyObject? in
            var localTask = task

            if ((localTask.result) != nil) {
                // Wait for up to 4 minutes until the table becomes ACTIVE.

                let describeTableInput = AWSDynamoDBDescribeTableInput()
                describeTableInput.tableName = AWSSampleDynamoDBTableName;
                localTask = dynamoDB.describeTable(describeTableInput)

                for _ in 0...15 {
                    localTask = localTask.continueWithSuccessBlock({ task -> AnyObject? in
                        let describeTableOutput:AWSDynamoDBDescribeTableOutput = task.result as! AWSDynamoDBDescribeTableOutput
                        let tableStatus = describeTableOutput.table!.tableStatus
                        if tableStatus == AWSDynamoDBTableStatus.Active {
                            return task
                        }

                        sleep(15)
                        return dynamoDB .describeTable(describeTableInput)
                    })
                }
            }

            return localTask
        })

    }
}*/

class DDBTableRow :AWSDynamoDBObjectModel ,AWSDynamoDBModeling  {
    
    var Address:String?
    var NumberOfRooms:NSNumber?
    var Owner:String?
    var PhotoURL:String?
    var Price:NSNumber?
    var Seller:String?
    class func dynamoDBTableName() -> String {
        return AWSSampleDynamoDBTableName
    }
    
    class func hashKeyAttribute() -> String {
        return "Address"
    }
    
    
    
    
    
}