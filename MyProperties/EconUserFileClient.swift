//
//  EconUserFileClient.swift
//  MyProperties
//
//  Created by Donncha Finlay on 30/09/16.
//  Copyright © 2016 Donncha Finlay. All rights reserved.
//


import Foundation
import UIKit


class EconUserFileClient : NSObject {
    
    // MARK: Constants
    struct Constants {
        //http://ccnotairebackendserverdev.eu-west-1.elasticbeanstalk.com:3333/v1/me/user?access_token=827f7850-8626-11e6-b0b4-29e86b51d6fc
        
        //http://ccnotairebackendserverdev.eu-west-1.elasticbeanstalk.com%3a3333/v1/me/user?access_token=827f7850-8626-11e6-b0b4-29e86b51d6fc
        // parameter keys
         static let TokenKey = "access_token"
        
        // parameter values
        static let TokenValue = "827f7850-8626-11e6-b0b4-29e86b51d6fc"
        
        
        // MARK: URLs
        static let ApiScheme = "http"
        static let ApiHost = "ccnotairebackendserverdev.eu-west-1.elasticbeanstalk.com"
        static let ApiPort = 3333
        static let ApiPath = "/v1/me"
        
    }
    // MARK: Methods
    struct Methods {
        
        // MARK: Link
        static let User = "/user"
     }
    
    // MARK: GET
    /*func taskForGETMethod(method: String, var parameters: [String:AnyObject], completionHandlerForGET: (result: AnyObject!, error: NSError?) -> Void) -> NSURLSessionDataTask {*/
    
    func taskForGETMethod(method: String, var parameters: [String:AnyObject], completionHandlerForGET: (result: AnyObject!, error: NSError?) -> Void) -> Void {
        // Note the completion handler is passed from the controller this will test for an error in parsed data or get required info
        // Get the stack
        let delegate = UIApplication.sharedApplication().delegate as! AppDelegate
        /* 1. Set the parameters  Manually */
        //let methodParameters = [Constants.TokenKey: Constants.TokenValue]
        // Or used Passed parameters
        // 2/3. build the url
        let request = NSURLRequest(URL:delegate.econocomDBURLFromParameters(parameters,withPathExtension: method ))
        //
        print(request)
        // 4. Make the request
        let task =  delegate.sharedSession.dataTaskWithRequest(request){ (data, response ,error) in
            //
            func sendError(error: String) {
                print(error)
                let userInfo = [NSLocalizedDescriptionKey : error]
                completionHandlerForGET(result: nil, error: NSError(domain: "taskForGETMethod", code: 1, userInfo: userInfo))
            }
            /* GUARD: Was there an error? */
            guard (error == nil) else {
                sendError("There was an error with your request: \(error)")
                return
            }
            
            /* GUARD: Did we get a successful 2XX response? */
            guard let statusCode = (response as? NSHTTPURLResponse)?.statusCode where statusCode >= 200 && statusCode <= 299 else {
                sendError("Your request returned a status code other than 2xx!")
                return
            }
            
            /* GUARD: Was there any data returned? */
            guard let data = data else {
                sendError("No data was returned by the request!")
                return
            }
            // 5. Parse Data
            let parsedResult : AnyObject!
            do{
                parsedResult = try NSJSONSerialization.JSONObjectWithData(data, options: .AllowFragments)
            } catch {
                self.displayError("Could not parse the data as JSON '\(data)'")
                return
            }
            guard let user = parsedResult["mail"] as? String else {
                self.displayError("Cannot find key response in result")
                return
            }
            print(parsedResult)
            /* 5/6. Parse the data and use the data (happens in completion handler) */
            // Note the completion handler is passed from the controller and will be called back
            self.convertDataWithCompletionHandler(data, completionHandlerForConvertData: completionHandlerForGET)
            
            
        }
        task.resume()
        //return nil
    }
    
    // given raw JSON, return a usable Foundation object
    private func convertDataWithCompletionHandler(data: NSData, completionHandlerForConvertData: (result: AnyObject!, error: NSError?) -> Void) {
        
        var parsedResult: AnyObject!
        do {
            parsedResult = try NSJSONSerialization.JSONObjectWithData(data, options: .AllowFragments)
        } catch {
            let userInfo = [NSLocalizedDescriptionKey : "Could not parse the data as JSON: '\(data)'"]
            completionHandlerForConvertData(result: nil, error: NSError(domain: "convertDataWithCompletionHandler", code: 1, userInfo: userInfo))
        }
        
        completionHandlerForConvertData(result: parsedResult, error: nil)
    }
    func displayError(errorString: String?) {
        if let errorString = errorString {
            print(errorString)
            //debugTextLabel.text = errorString
        }
    }
    
}